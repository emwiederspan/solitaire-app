//
//  ViewController.swift
//  Solitaire
//
//  Created by Ethan Wiederspan on 1/23/19.
//  Copyright © 2019 Pacific Lutheran University. All rights reserved.
//

import UIKit

class SolitaireViewController: UIViewController {
    
    private var game = Solitaire()
    private var previouslySelectedCardIndex: Int? = nil
    private var tableau: [[PlayingCardView]?] = [[]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        winLabel.text = ""
        newGameButton.isHidden = true
        
        // Order Views in tableau columns
        tableauColumnOne = tableauColumnOne.sorted(by: { $0.tag < $1.tag})
        tableauColumnTwo = tableauColumnTwo.sorted(by: { $0.tag < $1.tag})
        tableauColumnThree = tableauColumnThree.sorted(by: { $0.tag < $1.tag})
        tableauColumnFour = tableauColumnFour.sorted(by: { $0.tag < $1.tag})
        tableauColumnFive = tableauColumnFive.sorted(by: { $0.tag < $1.tag})
        tableauColumnSix = tableauColumnSix.sorted(by: { $0.tag < $1.tag})
        tableauColumnSeven = tableauColumnSeven.sorted(by: { $0.tag < $1.tag})
        
        // Put them into one master Array
        tableau = [tableauColumnOne, tableauColumnTwo, tableauColumnThree,
                   tableauColumnFour, tableauColumnFive, tableauColumnSix,
                   tableauColumnSeven]
        
        // Add tap gesture recognizers for tableau
        for column in tableau {
            column?.forEach({
                $0.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(touchCard(byHandlingGestureRecognizedBy:))))
            })
        }
        
        // Order views in foundation piles
        foundations = foundations.sorted(by: { $0.tag < $1.tag})
        
        // Add tap gesture recognizers for foundation
        foundations.forEach({
            $0.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(touchCard(byHandlingGestureRecognizedBy:))))
        })
        
        // Order views in hand
        hand = hand.sorted(by: { $0.tag < $1.tag})
        
        // Add tap gesture recognizers for hand
        hand.forEach({
            $0.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(touchCard(byHandlingGestureRecognizedBy:))))
        })
        
        updateViewFromModel()
    }
    
    @objc func touchCard(byHandlingGestureRecognizedBy recognizer: UITapGestureRecognizer){
        print("touched a card")
        // Get PlayingCardView
        if let cardView = recognizer.view as? PlayingCardView {
            switch recognizer.state {
            case .ended:
                
                // Select the card
                if game.chooseCard(at: cardView.tag){
                    if previouslySelectedCardIndex == cardView.tag{
                        // This is the previously selected card
                        print("its the previously selected one")
                        previouslySelectedCardIndex = nil
                        cardView.isSelected = false
                    }else if previouslySelectedCardIndex != nil {
                        // A defferent card was previously selected, try to move the card
                        
                        // Is the move valid?
                        if game.moveCard(from: previouslySelectedCardIndex!, to: cardView.tag){
                            // move is valid
                            print("\tMove is Valid....")
                        }else{
                            // move is invalid
                            print("\tMOVE IS NOT VALID")
                        }
                        
                        // Reset the previous card
                        previouslySelectedCardIndex = nil
                    }else{
                        // No other card is selected, just mark this as the preivously selected card
                        previouslySelectedCardIndex = cardView.tag
                    }
                }else{
                    print("card was not selected")
                    // Card can't be selected
                }
                updateViewFromModel()
            default:
                break
            }
        }
    }
    
    
    @IBOutlet weak var newGameButton: UIButton!
    
    @IBAction func deckButton(_ sender: UIButton) {
        
        if(game.draw()){
            updateViewFromModel()
        }else{
            // The deck is empty, make the deck disapear
            sender.setBackgroundImage(nil, for: .normal)
        }
    }
    
    @IBOutlet weak var winLabel: UILabel!
    
    @IBOutlet var hand: [PlayingCardView]!
    
    @IBOutlet var foundations: [PlayingCardView]!
    
    @IBOutlet var tableauColumnOne: [PlayingCardView]!
    @IBOutlet var tableauColumnTwo: [PlayingCardView]!
    @IBOutlet var tableauColumnThree: [PlayingCardView]!
    @IBOutlet var tableauColumnFour: [PlayingCardView]!
    @IBOutlet var tableauColumnFive: [PlayingCardView]!
    @IBOutlet var tableauColumnSix: [PlayingCardView]!
    @IBOutlet var tableauColumnSeven: [PlayingCardView]!
    
    @IBAction func touchNewGame(_ sender: Any) {
        winLabel.text = ""
        game = Solitaire()
        updateViewFromModel()
        newGameButton.isHidden = true
    }
    
    private func updateViewFromModel(){
        
        // Update the tableau
        for index in tableau.indices{
            updateTableauColumn(at: index)
        }
        
        // Update the foundation
        for index in foundations.indices{
            let cardView = foundations[index]
            // Check if the foundation has pile
            if let topCard = game.foundation[index].topCard{
                print("foundation has a card, displaying it")
                
                // Display that top card
                
                cardView.suit = topCard.suit.description
                cardView.rank = topCard.rank.order
                cardView.isSelected = game.foundation[index].isSelected
                cardView.hasCard = true
                cardView.isEmptySpot = false
                cardView.isFaceUp = true
            }else{
                // The Pile is empty
                cardView.isEmptySpot = true
                cardView.isSelected = false
                cardView.hasCard = false
            }
        }
        
        // Check foundation for the win
        if game.foundation[0].isComplete && game.foundation[1].isComplete &&
            game.foundation[2].isComplete && game.foundation[3].isComplete{
            newGameButton.isHidden = false
            winLabel.text = "You Win!"
        }
        
        
        // Update hand
        updateHand()
        
    }
    
    private func updateTableauColumn(at tableauIndex: Int ){
        
        let column = tableau[tableauIndex]!
        
        if game.tableau[tableauIndex].cardStack.count == 0 &&
            game.tableau[tableauIndex].hiddenCards.count == 0{
            print("updateTableauColumn: tableau \(tableauIndex) is empty!")
            let cardView = column[0]
            // Mark as empty spot
            cardView.isEmptySpot = true
            cardView.isSelected = game.tableau[tableauIndex].isSelected
            column.forEach({
                $0.hasCard = false
                $0.isSelected = false
            })
        }else{
            for index in column.indices{
                let cardView = column[index]
                cardView.isEmptySpot = false
                
                // If there is a card in the model, display the card
                if index < game.tableau[tableauIndex].cardStack.count{
                    
                    
                    let modelCard = game.tableau[tableauIndex].cardStack[index]
                    
                    cardView.suit = modelCard.suit.description
                    cardView.rank = modelCard.rank.order
                    cardView.isSelected = modelCard.isSelected
                    cardView.isFaceUp = true
                    cardView.hasCard = true
                }else{
                    // There is no card there
                    cardView.isSelected = false
                    cardView.hasCard = false
                }
            }
        }
    }
    
    private func updateHand(){
     
        
        for index in hand.indices{
            let cardView = hand[index]
            
            // If there is a card in the model, display the card
            if index < game.hand.count{
                let modelCard = game.hand[index]
                
                cardView.suit = modelCard.suit.description
                cardView.rank = modelCard.rank.order
                cardView.isSelected = modelCard.isSelected
                cardView.hasCard = true
                cardView.isFaceUp = true
            }else{
                // There is no card there
                cardView.isSelected = false
                cardView.hasCard = false
            }
        }
    }
}

