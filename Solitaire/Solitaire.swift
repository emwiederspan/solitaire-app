//
//  Solitaire.swift
//  Solitaire
//
//  Created by Ethan Wiederspan on 1/23/19.
//  Copyright © 2019 Pacific Lutheran University. All rights reserved.
//

import Foundation

class Solitaire {
    
    var deck: [PlayingCard]
    var tableau: [TableauColumn]
    var foundation: [FoundationPile]
    var hand: [PlayingCard]
    var discard: [PlayingCard]
    var avaliableCardFromHand: Int{
        return 94 + hand.count
    }
    
    init() {
        
        // Make the deck
        deck = [PlayingCard]()
        for suit in PlayingCard.Suit.all {
            for rank in PlayingCard.Rank.all {
                deck.append(PlayingCard(suit: suit, rank: rank, isSelected: false))
            }
        }
        deck.shuffle()
        
        // Make the initial tableau
        tableau = []
        for index in 0...6{
            
            var cards = [PlayingCard]()
            for _ in 0..<index{
                cards.append(deck.removeLast())
            }
            tableau.append(TableauColumn(hiddenCards: cards, cardStack: [deck.removeLast()]))
        }
        
        // Initialize the foundation
        foundation = []
        for _ in 0...3{
            foundation.append(FoundationPile())
        }
        
        // Initialize the hand and discard
        hand = []
        discard = []
    }
    
    func chooseCard(at index: Int) -> Bool{
        
        print("Soliare.chooseCard(at: \(index))")
        
        switch index {
        case 0...90:
            // Card is in the tableau
            print("\tcard is in the tableau")
            let tableauIndex = index/13
            let cardIndex = index - tableauIndex*13
            
            // If there are no cards there, select the stack
            if tableau[tableauIndex].cardStack.count == 0{
                tableau[tableauIndex].isSelected = !tableau[tableauIndex].isSelected
                return true
            }else{
                print("\t\tvalid")
                tableau[tableauIndex].cardStack[cardIndex].isSelected = !tableau[tableauIndex].cardStack[cardIndex].isSelected
                return true
            }
        case 91...94:
            // Card is in the foundation
            print("\tcard is in the foundation")
            foundation[index-91].isSelected = !foundation[index-91].isSelected
            return true
        case 95...97:
            // Card is in the hand
            print("\tcard is in the hand")
            
            // Make sure they are selecting the top card
            print("\tindex: \(index), avaliable: \(avaliableCardFromHand)")
            if(index == avaliableCardFromHand){
                print("\t\tvalid")
                hand[index-95].isSelected = !hand[index-95].isSelected
                return true
            }
        default:
            print("card selection error!")
            return false
        }
        return false
    }
    
    func moveCard(from startIndex: Int, to destinationIndex: Int) -> Bool{
    
        print("Solitaire.move(from: \(startIndex), to: \(destinationIndex))")
        
        // Deselect the cards
        deselectCardsAt([startIndex, destinationIndex])
        
        // Make sure the start index is valid
        if startIndex > 90 && startIndex < 95{
            // Movement from the foundation is invalid
            return false
        }
        
        // Determine the destination
        switch destinationIndex{
        case 0...90:
            // The attempted move is to the tableau
            return move(from: startIndex, toTableauCard: destinationIndex)
        case 91...94:
            // The attempted move is to the foundation
            return move(from: startIndex, toFoundationCard: destinationIndex)
        default:
            // The attempted move is invalid (to the hand or index error)
            return false
        }
    }
    
    private func move(from startIndex: Int, toTableauCard destinationIndex: Int) -> Bool{
        
        // Get cards at the indices
        let startingCard: PlayingCard
        if(startIndex < 91){
            // Starting card is in the tableau
            let tableauIndex = startIndex/13
            let cardIndex = startIndex - tableauIndex*13
            
            // Can't move an empty stack
            if tableau[tableauIndex].cardStack.count == 0{
                return false
            }
            
            startingCard = tableau[tableauIndex].cardStack[cardIndex]
        }else{
            // Starting card is in the hand
            startingCard = hand[startIndex - 95]
        }
        let destinationTableauIndex = destinationIndex/13
        
        // The tableau may be empty
        if tableau[destinationTableauIndex].cardStack.count == 0 &&
            tableau[destinationTableauIndex].hiddenCards.count == 0 &&
            startingCard.rank.order == 13{
            // If the tableau is entirely empty, you can put a King there
            print("\tmove to tableau was valid")
            // Get the stack of cards under the sender and move them
            let stack = getCardStack(at: startIndex)
            tableau[destinationTableauIndex].addCards(stack)
            return true
        }else if let destinationCard = tableau[destinationTableauIndex].bottomCard{
            // The tableau isn't empty, your card must match the rules
            
            // Check if the move was valid
            if startingCard.color != destinationCard.color{
                if startingCard.rank.order == destinationCard.rank.order - 1{
                    // The card is the opposite color and a rank lower
                    print("\tmove to tableau was valid")
                    // Get the stack of cards under the sender and move them
                    let stack = getCardStack(at: startIndex)
                    tableau[destinationTableauIndex].addCards(stack)
                    return true
                }
            }
        }
        // If you got here, the move was not valid
        return false
    }// move(toTableauCard)
    
    private func move(from startIndex: Int, toFoundationCard destinationIndex: Int) -> Bool{
        print("Solitaire.move(toFoundationCard:)")
        // Get starting card
        let card: PlayingCard
        if(startIndex < 91){
            // Starting card is in the tableau
            let tableauIndex = startIndex/13
            let cardIndex = startIndex - tableauIndex*13
            
            // Make sure the card is the bottom one on the stack
            if (cardIndex+1) != tableau[tableauIndex].cardStack.count ||
                tableau[tableauIndex].cardStack.count == 0{
                // You can't move a middle card to the foundation
                print("\tmove to foundation was not valid(middle)")
                return false
            }
            
            card = tableau[tableauIndex].cardStack[cardIndex]
        }else{
            // Starting card is in the hand
            card = hand[startIndex - 95]
        }
    
        // Get destination pile
        let pile = foundation[destinationIndex - 91]
        
        // Check if the move is valid for a foundation
        if let destinationCard = pile.topCard{
            if card.suit == destinationCard.suit{
                if card.rank.order - 1 == destinationCard.rank.order{
                    // The card is the same suit and one rank higher than the destination
                    print("\tmove to foundation was valid")
                    pile.addCard(getCardStack(at: startIndex)[0])
                    return true
                }
            }
        }else {
            // If the foundation is empty, you can put an Ace there
            if card.rank.order == 1{
                print("\tmove to foundation was valid")
                pile.addCard(getCardStack(at: startIndex)[0])
                return true
            }
        }
        
        print("\tmove to foundation was not valid")
        // If you get here, the move wasn't valid
        
        return false
    }
    
    private func getCardStack(at index: Int) -> [PlayingCard]{
        
        let returnStack: [PlayingCard]
        print("in Solitaire.getCardStack (removes cards) at: \(index)")
        if index < 91 {
            // Card is in the tableau, determine the tableau and card
            let tableauIndex = index/13
            let cardIndex = index - tableauIndex*13
            returnStack = Array(tableau[tableauIndex].cardStack[cardIndex...])
            tableau[tableauIndex].removeCards(startingAt: cardIndex)
        }else{
            // Card is in the hand, return array of just the one card
            returnStack = [hand.removeLast()]
            print("removing card from hand, now we have:\(hand.count)")
            // If hand becomes empty and there are other cards in the discard, display three
            if hand.count == 0{
                updateHandFromDiscard()
            }
        }
        
        return returnStack
    }
    
    func draw() -> Bool{
        print("Solitaire.draw, discard: \(discard.count)")
        
        // Add hand to discard
        
        
        // Check if there are enough cards in the deck
        switch deck.count{
        case 4...:
            discard += hand
            hand = []
            hand = [deck.removeLast(), deck.removeLast(), deck.removeLast()]
            print("After: Solitaire.draw, discard: \(discard.count)")
            return true
        case 1...3:
            // Only draw as many as there are cards
            discard += hand
            hand = []
            for _ in deck{
                hand.append(deck.removeLast())
            }
            print("After: Solitaire.draw(1...3), discard: \(discard.count)")
            return true
        case 0:
            // not enough cards, put cards back in
            if discard.count != 0{
                refillDeck()
                print("After: Solitaire.draw(0), discard: \(discard.count)")
                return true
            }else{
                return false
            }
        default:
            return false
        }
    }
    
    private func updateHandFromDiscard(){
        print("Solitaire.updateHandFromDiscard, discard has \(discard.count)")
        switch discard.count{
        case 4...:
            // get the top three of the discard
            hand = []
            hand = [discard.removeLast(), discard.removeLast(), discard.removeLast()]
            hand = hand.reversed()
        case 1...3:
            // Only draw as many as there are cards
            hand = discard
            discard = []
        case 0:
            // not enough cards, put cards back in
            if hand.count != 0 && discard.count != 0{
                refillDeck()
            }else{
                
            }
        default:
            break
        }
    }
    
    func refillDeck(){
        print("solitaire.refillDeck, discard: \(discard.count), deck: \(deck.count), hand: \(hand.count)")
        
        discard += hand
        deck = discard.reversed()
        discard = []
        hand = []
    }
    
    private func deselectCardsAt(_ cardIndicies: [Int]){
        
        print("Solitaire.deselectCards")
        
        for index in cardIndicies{
            switch index{
            case 0...90:
                let tableauIndex = index/13
                let cardIndex = index - tableauIndex*13
                if tableau[tableauIndex].isSelected{
                    tableau[tableauIndex].isSelected = false
                }else{
                    tableau[tableauIndex].cardStack[cardIndex].isSelected = false
                }
                
                break
            case 91...94:
                foundation[index - 91].isSelected = false
                break
            case 95...97:
                hand[index - 95].isSelected = false
                break
            default:
                print("\tDESELECT ERROR")
                break
            }
            
        }
    }
}

class TableauColumn {
    var hiddenCards: [PlayingCard]
    var isSelected: Bool = false
    var cardStack: [PlayingCard] {
        didSet{
            // I don't know if this will work
            if cardStack.count == 0{
                if hiddenCards.count != 0{
                    self.flipCard()
                }
            }
        }
    }
    
    var bottomCard: PlayingCard?{
        return cardStack.last
    }
    
    init(hiddenCards hc: [PlayingCard], cardStack cs: [PlayingCard]){
        self.hiddenCards = hc
        self.cardStack = cs
    }
    
    func addCards(_ stack: [PlayingCard]){
        for card in stack{
            cardStack.append(card)
        }
    }
    
    func removeCards(startingAt startIndex: Int){
        for _ in startIndex...cardStack.count-1{
            cardStack.removeLast()
        }
    }
    
    func flipCard(){
        if hiddenCards.count > 0{
            print("TableauColumn.flipCard: Hidden: \(hiddenCards.count)")
            addCards([hiddenCards.removeLast()])
            
        }else{
            print("no more hidden cards!")
        }
    }
}

class FoundationPile {
    var cardStack: [PlayingCard]
    var isSelected: Bool
    var topCard: PlayingCard?{
        return cardStack.last
    }
    var isComplete: Bool{
        return cardStack.count == 14
    }
    
    init(){
        cardStack = []
        isSelected = false
    }
    
    func addCard(_ card: PlayingCard){
        cardStack.append(card)
    }
    
}

